
module NewApp
  
  dir = File.dirname __FILE__
  require File.expand_path('./common/constants', dir)
  require File.expand_path('./common/config', dir)
  require File.expand_path('./common/version', dir)
  require File.expand_path('./common/logger', dir)
  
  # Agent Main Class
  class Agent
  
    include ErrorCodes
  
    def initialize
      # Your code goes here...
      NewApp::MyConfig.new
      $logger = NewApp::MyLogger.new
      $version = (Version::VERSION)

      $logger.info "Version: #{$version}"
      msg = "Server Runs Every: " + ($sleep_Interval/60).to_s + " minutes"
      $logger.info msg
      msg = "Logging Level: " + $loglevel.to_s
      $logger.info msg
    end
    
    def start
      require 'webrick'
      require 'xmlrpc/server'
      
      xml_servlet = XMLRPC::WEBrickServlet.new
      xml_servlet.add_handler("convert_celcius") do |celcius|
        celcius*1.8 + 32
      end
      
      xml_servlet.add_multicall # Add support for multicall
      agent = WEBrick::HTTPServer.new(:Port => 2000)
      agent.mount("/RPC2", xml_servlet)
      trap("INT"){ agent.shutdown }
      agent.start
    end

  end
  
end
