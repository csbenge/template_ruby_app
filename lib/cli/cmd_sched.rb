# cmd_sched.rb - run the schedule command

module NewApp
  
  dir = File.dirname __FILE__
  require File.expand_path('../common/constants', dir)
  require File.expand_path('../common/cron', dir)
  
  class Sched
    
    include ErrorCodes
    
    def initialize
    end
    
    def run(options)
      begin
        cronjob = Cron.new(options[:cronspec])
      rescue
        puts "sched: Invalid CRONSPEC!"
        return FAIL
      end
      
      if (command = options[:command]) == nil
        puts "sched: no COMMAND specficied!"
        return FAIL
      end
      
      curr_time = Time.now
      
      next_comming_time   = cronjob.next(curr_time)
      #most_resently_time  = cronjob.last(curr_time)

      msg = "sched: command " + "'" + command + "'" + " to run at " + "'" + options[:cronspec] + "'"
      $logger.info msg
      msg = "sched: first run: " + next_comming_time.ctime
      $logger.info msg
      return PASS
    end

  end
   
end
