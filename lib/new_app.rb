#
# == Synopsis 
#   This is a sample description of the application.
#   Blah blah blah.
#
# == Examples
#   This command does blah blah blah.
#     new_app sched -s "30 * * * *" -c "ls -F"
#
#   Other examples:
#     new_app sched -s "30 * * * *" -c "ls -F"
#
# == Usage 
#   new_app COMMAND [options]
#
#   For help use: new_app -h
#
# == Commands
#   sched          Schedule a command to run
#
# == Options
#   -h, --help          Displays help message
#   -v, --version       Display the version, then exit
#   -e, --environment   Output as little as possible, overrides verbose
#   -d, --daemon        Verbose output
#   -s, --cronspec      A valid cronspec
#   -c, --command       Command to run
#
# == Author
#   Carey Benge
#
# == Copyright
#   Copyright (c) 2013 Carey Benge.
#

module NewApp
  
  dir = File.dirname __FILE__
  require File.expand_path('./common/constants', dir)
  require File.expand_path('./common/config', dir)
  require File.expand_path('./common/version', dir)
  require File.expand_path('./common/logger', dir)
  
  require File.expand_path('./cli/cmd_sched', dir)
  
  # New_App Main Class
  class Main
  
    include ErrorCodes
  
    def initialize(mode)
      NewApp::MyConfig.new
      $logger = NewApp::MyLogger.new
      $version = (Version::VERSION)
      $logger.info "STARTING New_App Service..."
      $logger.info "Version: #{$version}"
      
      if mode == "interactive"
        $interactive = TRUE
        $logger.info "Interactive: TRUE"
      else
        $interactive = FALSE
        $logger.info "Interactive: FALSE"
      end

      msg = "Engine Runs Every: " + ($sleep_Interval/60).to_s + " minutes"
      $logger.info msg
      msg = "Logging Level: " + $loglevel.to_s
      $logger.info msg
  
      $logger.debug "Version: #{$version}"
      $logger.warn "Version: #{$version}"
      $logger.fatal "Version: #{$version}"
      $logger.info "STARTED New_App Service..."
    end

    # Execute command with options
    def run(*argv)
      require 'optparse'

      options = {}
  
      opt_parser = OptionParser.new do |opt|
        opt.banner = "Usage: new_app COMMAND [OPTIONS]"
        opt.separator  ""
        opt.separator  "Commands"
        opt.separator  "     sched: schedule a command to run"
        opt.separator  "     start: start server"
        opt.separator  ""
        opt.separator  "Options"

        opt.on("-h","--help","help") do
          puts opt_parser
          exit
        end

        opt.on("-v","--version","display application version") do
          msg = "new_app: version: #{$version}"
          if $interactive == TRUE
            puts msg
          else
            return msg
          end
          exit
        end
  
        opt.on("-e","--environment ENVIRONMENT","which environment you want server run") do |environment|
          options[:environment] = environment
        end
  
        opt.on("-d","--daemon","running on daemon mode?") do
          options[:daemon] = true
        end
  
        opt.on("-c","--command Command","command for scheduling") do |command|
          options[:command] = command
        end
  
        opt.on("-s","--cronspec CRONSPEC","cronspec for scheduling") do |cronspec|
          options[:cronspec] = cronspec
        end

      end
  
      opt_parser.parse!
  
      # Run a command
      case argv[0]
        when "sched"
          sched = Sched.new
          rc = sched.run(options)
          
          if $interactive == TRUE
            print "Job Scheduled: " if rc == PASS
            print "FAILED to Schedule Job: " if rc == FAIL
            puts "#{options.inspect}"
            puts opt_parser if rc == FAIL
          else
            return "Job Scheduled: #{options.inspect}" if rc == PASS
            return "FAILED to Schedule Job:  #{options.inspect}" if rc == FAIL
          end
          
        when "start"
          if $interactive == TRUE
            puts "call start on options #{options.inspect}"
          else
            return "call start on options #{options.inspect}"
          end
        else
          puts opt_parser
      end
    end
  
  end
  
end
