# api.rb - api service

module NewApp
  
  dir = File.dirname __FILE__
  require File.expand_path('./common/constants', dir)
  require File.expand_path('./common/config', dir)
  require File.expand_path('./common/version', dir)
  require File.expand_path('./common/logger', dir)
  require File.expand_path('./new_app', dir)
  
  # Api Main Class
  class Api
  
    include ErrorCodes
    
    @@api_call_count = 0
  
    def initialize
      NewApp::MyConfig.new
      $logger = NewApp::MyLogger.new
      $version = (Version::VERSION)

      $logger.info "STARTING API Service..."
      $logger.info "Version: #{$version}"
      msg = "Logging Level: " + $loglevel.to_s
      $logger.info msg
      @newapp = NewApp::Main.new("api")
      $logger.info "STARTED API Service..."
     
    end
    
    def start
      require 'webrick'
      require 'xmlrpc/server'
      
      xml_servlet = XMLRPC::WEBrickServlet.new
      xml_servlet.add_handler("convert_celcius") do |command|
        run(command)
      end
      
      xml_servlet.add_multicall # Add support for multicall
      api = WEBrick::HTTPServer.new(:Port => 2001)
      api.mount("/RPC2", xml_servlet)
      trap("INT"){ api.shutdown }
      api.start
    end
    
    def run(*args)
      puts "RUN"
      @@api_call_count += 1
      msg = "[" +  @@api_call_count.to_s + "]: " + "API Service command=" + args[0]
      $logger.info msg
      @newapp.run args[0]
    end

  end
  
end
