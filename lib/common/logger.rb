# logger.rb - logging functions

module NewApp

  require 'logger'
  
  # Application Logger initialize and invoke
  class MyLogger
    
    def initialize
      time = Time.now
      now = time.strftime("%Y-%m-%d-%H.%M,%S") # 07/24/2006 09:09:03
      
      @log = Logger.new("../log/#{$app_name}-#{now}.log")
      @log.formatter = proc do |severity, datetime, progname, msg|
        "#{datetime} - #{severity} - #{msg}\n"
      end
    end
      
    # Logs an INFO message
    def info(msg)
      @log.info msg if $loglevel > 0
    end
    
     # Logs an WARN message
    def warn(msg)
      @log.warn msg if $loglevel > 1
    end
    
     # Logs an DEBUG message
    def debug(msg)
      @log.debug msg if $loglevel > 2
    end
    
     # Logs an FATAL message
    def fatal(msg)
      @log.fatal msg
    end
    
  end

end