# constants.rb - application constants

module NewApp
  
  TRUE  = 1
  FALSE = 0
  
  module ErrorCodes
    PASS      = 0
    FAIL      = 1
    WARN      = 2
  end
  
end
