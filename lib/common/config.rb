# config.rb - configuration Settings

module NewApp

  require 'yaml'
   
  class MyConfig

    # Get configuration settings from conf/config.yml
    def initialize
      config      = YAML.load_file("../conf/config.yml")
      
      $loglevel         = config["datical"]["loglevel"]
      $sleep_Interval   = config["datical"]["sleep_interval"].to_i * 60
      $workspace        = config["datical"]["workspace_dir"]
      $datical_cli_dir  = config["datical"]["datical_cli_dir"]
      
      $app_name = $0.split("/").last
      
    end
  
  end

end

