
module NewApp
  
  dir = File.dirname __FILE__
  require File.expand_path('./common/constants', dir)
  require File.expand_path('./common/config', dir)
  require File.expand_path('./common/version', dir)
  require File.expand_path('./common/logger', dir)
  
  # Server Main Class
  class Server
  
    include ErrorCodes
  
    def initialize
      # Your code goes here...
      NewApp::MyConfig.new
      $logger = NewApp::MyLogger.new
      $version = (Version::VERSION)

      $logger.info "Version: #{$version}"
      msg = "Server Runs Every: " + ($sleep_Interval/60).to_s + " minutes"
      $logger.info msg
      msg = "Logging Level: " + $loglevel.to_s
      $logger.info msg
    end
    
    def run(*argv)
     require 'xmlrpc/client'
      server = XMLRPC::Client.new("localhost", "/RPC2", 2000)
      puts server.call("convert_celcius", 0)
      puts server.call("convert_celcius", 100)
      puts server.multicall(['convert_celcius', -10], ['convert_celcius', 200])
    end

  end
  
end
