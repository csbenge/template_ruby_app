#
# == Synopsis 
#   This is a sample description of the application.
#
# == Examples
#   This command does blah blah blah.
#     $APP_NAME start -c my_command -environment TEST -daemon
#
# == Usage 
#   $APP_NAME COMMAND [options]
#
#   For help use: $APP_NAME -h
#
# == Commands
#   start          Start something
#   stop           Stop something
#
# == Options
#   -c, --command       Command to start/stop
#   -h, --help          Displays help message
#   -v, --version       Display the version, then exit
#   -e, --environment   Output as little as possible, overrides verbose
#   -d, --daemon        Verbose output
#   -s, --server        Server name
#
# == Author
#   Carey Benge
#
# == Copyright
#   Copyright (c) 2014 Carey Benge. All Rights Reserved.
#

module Application
  
  VERSION = "0.0.1"
  
  class Main
  
    def initialize(*argv)
      require 'optparse'

      $options = {}
  
      $opt_parser = OptionParser.new do |opt|
        opt.banner = "Usage: #{$PROGRAM_NAME} COMMAND [OPTIONS]"
        opt.separator  ""
        opt.separator  "Commands:"
        opt.separator  "     start: start something"
        opt.separator  "     stop:  stop something"
        opt.separator  ""
        opt.separator  "Options:"

	opt.on("-c","--command COMMAND","the command to start/stop") do |command|
          $options[:command] = command
        end
                  
        opt.on("-d","--daemon","run in daemon mode") do
          $options[:daemon] = true
        end
                
          opt.on("-e","--environment ENVIRONMENT","the environment to use") do |environment|
          $options[:environment] = environment
        end
                
        opt.on("-h","--help","help") do
          puts opt_parser
          exit
        end
  
        opt.on("-s","--server SERVER_NAME","the server you want to operate") do |server|
          $options[:server] = server
        end
                
        opt.on("-v","--version","display application version") do
          msg = "#{$PROGRAM_NAME}: version: #{VERSION}"
          puts msg
          exit
        end   

      end
      
      begin
        $opt_parser.parse!
      rescue
        msg = "ERROR: #{$PROGRAM_NAME}: improper options!"
        puts msg
        puts $opt_parser
        exit!
      end
    end

    #
    # Execute command with options
    #
    def run(*argv)
      case argv[0]
        when "start"
          start($options)
        when "stop"
          stop($options)
        else
          puts $opt_parser
      end
    end # method.run
    
    #
    # Command Implementations
    #    
    def start(options)
      puts "start called with options #{options.inspect}"
      if options.empty? 
        puts "Required Args: -c"
      end
      
    end
    
    def stop(options)
      puts "stop called with options #{options.inspect}"
    end
  
  end # class.Main

  app = Main.new
  app.run *ARGV

end # module.Application