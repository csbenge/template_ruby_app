# tc_cron.rb - test cron functions
 
module NewApp

dir = File.dirname __FILE__
require File.expand_path('../lib/new_app/cron', dir)

require "test/unit"
 
  class TestCron < Test::Unit::TestCase
 
    def test_pass
      assert_nothing_raised do
        Cron.new("15 * * * *")
      end
    end
    
    def test_next_run
      assert_nothing_raised do
        current_time = Time.now
        cronjob = Cron.new("15 * * * *")
        next_run_time = cronjob.next(current_time)
        if next_run_time <= current_time
          raise RuntimeError, "next_run_time computation error!"
        end
      end
    end
 
    def test_fail
      assert_raise ArgumentError do
        Cron.new("15 * * *")
      end
    end
 
  end 

end