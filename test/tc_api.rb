# tc_api.rb - test api service functions
 
module NewApp

#dir = File.dirname __FILE__

require "test/unit"
 
  class TestApi < Test::Unit::TestCase

    def test_api
      assert_nothing_raised do
        require 'xmlrpc/client'
        server = XMLRPC::Client.new("localhost", "/RPC2", 2001) # api service running?
        #assert_equal("TEst", server.call("convert_celcius", "start"))   # api working correctly?
        assert_equal("TEst", server.call("convert_celcius", "sched -s '30 * * * *' -c 'ls -F'"))   # api working correctly?
      end 
    end

  end 

end